// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \ingroup RichardsNCMimModel, Mineralization
 * \brief Adaption of the fully implicit scheme to the
 *        two-phase n-component fully implicit model with additional solid/mineral phases.
 */
#ifndef DUMUX_RICHARDSNCMIM_MODEL_HH
#define DUMUX_RICHARDSNCMIM_MODEL_HH

#include <dumux/porousmediumflow/richardsnc/model.hh>
#include <dumux/porousmediumflow/richardsnc/volumevariables.hh>

#include <dumux/material/solidstates/compositionalsolidstate.hh>

#include <dumux/porousmediumflow/mineralization/model.hh>
#include <dumux/porousmediumflow/mineralization/localresidual.hh>
#include <dumux/porousmediumflow/mineralization/volumevariables.hh>
#include <dumux/porousmediumflow/mineralization/vtkoutputfields.hh>

namespace Dumux {
namespace Properties {
//////////////////////////////////////////////////////////////////
// Type tags
//////////////////////////////////////////////////////////////////
NEW_TYPE_TAG(RichardsNCMim, INHERITS_FROM(RichardsNC));

//////////////////////////////////////////////////////////////////
// Property tags for the isothermal RichardsNCMim model
//////////////////////////////////////////////////////////////////

// use the mineralization local residual
SET_TYPE_PROP(RichardsNCMim, LocalResidual, MineralizationLocalResidual<TypeTag>);

//! use the mineralization volume variables together with the richardsnc vol vars
SET_PROP(RichardsNCMim, VolumeVariables)
{
private:
    using PV = typename GET_PROP_TYPE(TypeTag, PrimaryVariables);
    using FSY = typename GET_PROP_TYPE(TypeTag, FluidSystem);
    using FST = typename GET_PROP_TYPE(TypeTag, FluidState);
    using SSY = typename GET_PROP_TYPE(TypeTag, SolidSystem);
    using SST = typename GET_PROP_TYPE(TypeTag, SolidState);
    using MT = typename GET_PROP_TYPE(TypeTag, ModelTraits);
    using PT = typename GET_PROP_TYPE(TypeTag, SpatialParams)::PermeabilityType;

    using Traits = RichardsVolumeVariablesTraits<PV, FSY, FST, SSY, SST, PT, MT>;
    using NonMinVolVars = RichardsNCVolumeVariables<Traits>;
public:
    using type = MineralizationVolumeVariables<Traits, NonMinVolVars>;
};

//! Set the vtk output fields specific to this model
SET_TYPE_PROP(RichardsNCMim, VtkOutputFields, MineralizationVtkOutputFields<RichardsNCVtkOutputFields>);

//! The 2pnc model traits define the non-mineralization part
SET_PROP(RichardsNCMim, ModelTraits)
{
private:
    //! we use the number of components specified by the fluid system here
    using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem);
    static_assert(FluidSystem::numPhases == 2, "Only fluid systems with 2 fluid phases are supported by the 2p-nc model!");
    using SolidSystem = typename GET_PROP_TYPE(TypeTag, SolidSystem);
    using NonMineralizationTraits = RichardsNCModelTraits<FluidSystem::numComponents,
                                                      GET_PROP_VALUE(TypeTag, UseMoles),
                                                      GET_PROP_VALUE(TypeTag, PhaseIdx)>;
public:
    using type = MineralizationModelTraits<NonMineralizationTraits, SolidSystem::numComponents, SolidSystem::numInertComponents>;
};

//! The two-phase model uses the immiscible fluid state
SET_PROP(RichardsNCMim, SolidState)
{
private:
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using SolidSystem = typename GET_PROP_TYPE(TypeTag, SolidSystem);
public:
    using type = CompositionalSolidState<Scalar, SolidSystem>;
};

} // end namespace Properties
} // end namespace Dumux

#endif
