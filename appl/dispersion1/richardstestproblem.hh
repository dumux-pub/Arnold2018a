// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup RichardsNCTests
 * \brief Capillarity of Water and Contaminant into a high-permeability domain which uses the
 *        Richards box model.
 */
#ifndef DUMUX_RICHARDS_NC_TEST_PROBLEM_HH
#define DUMUX_RICHARDS_NC_TEST_PROBLEM_HH

#include <dumux/material/spatialparams/fv.hh>
#include <dumux/material/fluidmatrixinteractions/2p/vangenuchten.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>

#include <dumux/discretization/cellcentered/tpfa/properties.hh>
#include <dumux/discretization/box/properties.hh>
#include <dumux/porousmediumflow/problem.hh>
#include <dumux/porousmediumflow/richardsnc/model.hh>
#include <dumux/porousmediumflow/richardsncmim/mimlocalresidual.hh>
#include <dumux/porousmediumflow/richardsncmim/dispersionlocalresidual.hh>

#include "richardstestspatialparams.hh"

namespace Dumux
{
/*!
 * \ingroup RichardsNCTests
 * \brief A water infiltration problem into a high-permeability domain which uses the
 *        Richards box model.
 */
template <class TypeTag>
class RichardsTestProblem;


// Specify the properties for the problem
namespace Properties
{
NEW_TYPE_TAG(RichardsTestTypeTag, INHERITS_FROM(RichardsNC));
NEW_TYPE_TAG(RichardsTestBoxTypeTag, INHERITS_FROM(BoxModel, RichardsTestTypeTag));
NEW_TYPE_TAG(RichardsTestCCTypeTag, INHERITS_FROM(CCTpfaModel, RichardsTestTypeTag));

// Use 2d YaspGrid
SET_TYPE_PROP(RichardsTestTypeTag, Grid, Dune::YaspGrid<2>);

// Set the physical problem to be solved
SET_TYPE_PROP(RichardsTestTypeTag, Problem, RichardsTestProblem<TypeTag>);

// Set the physical problem to be solved
SET_TYPE_PROP(RichardsTestTypeTag, PointSource, SolDependentPointSource<TypeTag>);

// Set the MIM Dispersion local residual
SET_TYPE_PROP(RichardsTestTypeTag, LocalResidual, DispersionLocalResidual<TypeTag>);

//the spatial params
SET_TYPE_PROP(RichardsTestTypeTag, SpatialParams, RichardsTestSpatialParams<TypeTag>);
    

}

/*!
 * \ingroup RichardsNCModel
 * \ingroup ImplicitTestProblems
 *
 * \brief A water infiltration problem into a high-permeability domain which uses the
 *        Richards model.
 *
 * The domain is box shaped. Left, right and the top boundaries are Neumann
 * boundaries are closed (Neumann 0 boundary), the bottom boundary with fixed water pressure
 * (hydostatic, gradient from right to left). The water is pulled upwards by capillarity in the domain.
 * which uses the TwoPBoxModel, with the main difference being that
 * the domain is initally less saturated by water.
 *
 * This problem uses the \ref RichardsNCModel
 *
 * To run the simulation execute the following line in shell:
 * <tt>./test_boxrichardsnc -ParameterFile test_boxrichardsnc.input -TimeManager.TEnd 10000000</tt>
 * <tt>./test_ccrichardsnc -ParameterFile test_ccrichardsnc.input -TimeManager.TEnd 10000000</tt>
 *
 * where the initial time step is 100 seconds, and the end of the
 * simulation time is 10,000,000 seconds (115.7 days)
 */
template <class TypeTag>
class RichardsTestProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using Problem = typename GET_PROP_TYPE(TypeTag, Problem);
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using PrimaryVariables = typename GET_PROP_TYPE(TypeTag, PrimaryVariables);
    using FVElementGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry)::LocalView;
    using ElementVolumeVariables = typename GET_PROP_TYPE(TypeTag, GridVolumeVariables)::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using BoundaryTypes = typename GET_PROP_TYPE(TypeTag, BoundaryTypes);
    using NumEqVector = typename GET_PROP_TYPE(TypeTag, NumEqVector);
    using PointSource = typename GET_PROP_TYPE(TypeTag, PointSource);
    using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using Indices = typename GET_PROP_TYPE(TypeTag, ModelTraits)::Indices;
    using FVGridGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry);
    using SolutionVector = typename GET_PROP_TYPE(TypeTag, SolutionVector);
    using GridVariables = typename GET_PROP_TYPE(TypeTag, GridVariables);
    enum {
        pressureIdx = Indices::pressureIdx,
        compIdx = Indices::compMainIdx + 1,
        wPhaseIdx =FluidSystem::liquidPhaseIdx,
        conti0EqIdx = Indices::conti0EqIdx,

        dimWorld = GridView::dimensionworld
    };
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = Dune::FieldVector<Scalar, dimWorld>;

public:
    /*!
     * \brief Constructor
     *
     * \param timeManager The Dumux TimeManager for simulation management.
     * \param gridView The grid view on the spatial domain of the problem
     */
    RichardsTestProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry)
    {
        name_ = getParam<std::string>("Problem.Name");
        contaminantMoleFraction_ = getParam<Scalar>("Problem.ContaminantMoleFraction");
        totalSaturationColumn_ = getParam<Scalar>("Problem.TotalSaturationColumn");
    }

    void postTimeStep(const SolutionVector& curSol,
                      const GridVariables& gridVariables,
                      const Scalar timeStepSize)

    {
        // compute the mass in the entire domain to make sure the tracer is conserved
        Scalar tracerMass = 0.0;

        // bulk elements
        for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->fvGridGeometry());
            fvGeometry.bindElement(element);

            auto elemVolVars = localView(gridVariables.curGridVolVars());
            elemVolVars.bindElement(element, fvGeometry, curSol);

            for (auto&& scv : scvs(fvGeometry))
            {
                const auto& volVars = elemVolVars[scv];
                tracerMass += volVars.massFraction(wPhaseIdx, compIdx)*volVars.density(wPhaseIdx)
                              * scv.volume() * volVars.saturation(wPhaseIdx) * volVars.porosity() * volVars.extrusionFactor();

                accumulatedSource_ += this->scvPointSources(element, fvGeometry, elemVolVars, scv)[compIdx]
                                       * scv.volume() * volVars.extrusionFactor()
                                       * FluidSystem::molarMass(compIdx)
                                       * timeStepSize;
            }
        }

        std::cout << "\033[1;33m" << "The domain contains " << tracerMass*1e9 << " µg tracer, "
                  <<  accumulatedSource_*1e9 << " µg ("<< int(std::round(-accumulatedSource_/(tracerMass - accumulatedSource_)*100))
                  <<"%) was already extracted (balanced: "
                  <<  (tracerMass - accumulatedSource_)*1e9 << " µg)\033[0m" << '\n';
    }

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief The problem name
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string& name() const
    { return name_; }

    /*!
     * \brief Returns the temperature [K] within a finite volume
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature() const
    { return 273.15 + 10; }; // -> 10°C

    /*!
     * \brief Returns the reference pressure [Pa] of the non-wetting
     *        fluid phase within a finite volume
     *
     * This problem assumes a constant reference pressure of 1 bar.
     */
    Scalar nonWettingReferencePressure() const
    { return 1.0e5; };

    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param globalPos The position for which the boundary type is set
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes bcTypes;
        if (onLeftBoundary_(globalPos) || onRightBoundary_(globalPos) || onUpperBoundary_(globalPos))
            bcTypes.setAllNeumann();
        else
            bcTypes.setAllDirichlet();
        return bcTypes;
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        boundary segment.
     *
     * \param globalPos The position for which the Dirichlet value is set
     *
     * For this method, the \a values parameter stores primary variables.
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    { return initial_(globalPos); }


    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each phase. Negative values mean influx.
     *
     * \param globalPos The position for which the Neumann value is set
     */
    NumEqVector neumannAtPos(const GlobalPosition &globalPos) const
    { return NumEqVector(0.0); }


    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluate the initial values for a control volume.
     *
     * For this method, the \a values parameter stores primary
     * variables.
     *
     * \param globalPos The position for which the boundary type is set
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    { return initial_(globalPos); };

    // \}

private:
    PrimaryVariables initial_(const GlobalPosition &globalPos) const
    {
        const auto xTracer = [&,this]()
        {
            const GlobalPosition contaminationPos(
              {
                0.5 * this->fvGridGeometry().bBoxMax()[0],
                0 * this->fvGridGeometry().bBoxMax()[1]
              }
            );
            if ((globalPos - contaminationPos).two_norm() < 0.1*(this->fvGridGeometry().bBoxMax()-this->fvGridGeometry().bBoxMin()).two_norm() + eps_)
                return contaminantMoleFraction_;
            else
                return 0.0;
        }();


        PrimaryVariables values(0.0);
        // saturation of the column
        //totalSaturationColumn_ = getParam<Scalar>("Problem.TotalSaturationColumn");
        //const Scalar sw = 0.15;
        using MaterialLaw = typename ParentType::SpatialParams::MaterialLaw;
        const Scalar pc = MaterialLaw::pc(this->spatialParams().materialLawParamsAtPos(globalPos), totalSaturationColumn_);
        //! hydrostatic pressure profile
        values[pressureIdx] = nonWettingReferencePressure() - pc;
        if (onLowerBoundary_(globalPos))
        {
          values[pressureIdx] = nonWettingReferencePressure();
        }
        values[compIdx] = xTracer;
        return values;
    }

    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[0] < this->fvGridGeometry().bBoxMin()[0] + eps_;
    }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[0] > this->fvGridGeometry().bBoxMax()[0] - eps_;
    }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[1] < this->fvGridGeometry().bBoxMin()[1] + eps_;
    }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[1] > this->fvGridGeometry().bBoxMax()[1] - eps_;
    }

    static constexpr Scalar eps_ = 1.5e-7;
    std::string name_;
    Scalar contaminantMoleFraction_;
    Scalar pumpRate_;
    Scalar pcTop_;
    Scalar accumulatedSource_;
    Scalar totalSaturationColumn_;
};

} //end namespace Dumux

#endif
