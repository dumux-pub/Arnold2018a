SUMMARY
=======
This is the DuMuX module containing the code for producing the results
submitted for:

H. Arnold<br>
Modelling hydrodynamic dispersion under two-phase flow conditions<br>
Master's Thesis, 2018<br>
Universität Stuttgart

Installation
============

The easiest way to install this module is to create a new folder and to execute the file
[installArnold2018a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Arnold2018a/raw/master/installArnold2018a.sh)
in this folder.

```bash
mkdir -p Arnold2018a && cd Arnold2018a
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/Arnold2018a/raw/master/installArnold2018a.sh
sh ./installArnold2018a.sh
```

For a detailed information on installation have a look at the DuMuX installation
guide or use the DuMuX handbook, chapter 2.


Used Versions and Software
==========================

For an overview on the used versions of the DUNE and DuMuX modules, please have a look at
[installArnold2018a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Arnold2018a/raw/master/installArnold2018a.sh).


Installation with Docker
========================

Create a new folder in your favourite location and change into it

```bash
mkdir Arnold2018a
cd Arnold2018a
```

Download the container startup script by running
```bash
wget https://git.iws.uni-stuttgart.de/dumux-pub/Arnold2018a/-/raw/master/docker_arnold2018a.sh
```

Open the Docker Container
```bash
bash docker_arnold2018a.sh open
```

After the script has run successfully, you may build all executables

```bash
cd dumux-Arnold2018a/build-cmake
make build_tests
```

and you can run them individually. They are located in the build-cmake folder according to the following paths

- appl/twoptracerdispersion
- appl/tracerdispersion

They can be executed with an input file e.g., by running

```bash
cd appl/twoptracerdispersion
./tracer2p tracer2p.input
```
